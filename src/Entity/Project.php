<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlSimplon;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $urlGitlab;

    public function getId()
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getUrlSimplon(): ?string
    {
        return $this->urlSimplon;
    }

    public function setUrlSimplon(?string $urlSimplon): self
    {
        $this->urlSimplon = $urlSimplon;

        return $this;
    }

    public function getUrlGitlab(): ?string
    {
        return $this->urlGitlab;
    }

    public function setUrlGitlab(?string $urlGitlab): self
    {
        $this->urlGitlab = $urlGitlab;

        return $this;
    }
}
