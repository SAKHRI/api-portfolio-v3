<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;


class FileService {

    public function upload(File $file, string $absolutePath = ""):string {

        $path = __DIR__."/../../public/upload";

        //On vérifie si le dossier existe, si non, on le crée
        if(!is_dir($path)) {
            mkdir($path);
        }
        //On génère un nom de fichier unique avec l'extension qu'il faut
        $filename = uniqid() . "." . $file->guessExtension();
        //On met le fichier dans le dossier qu'on a spécifié au dessus
        $file->move($path, $filename);
        //On return le chemin éventuellement en absolu vers le fichier
        return $absolutePath . "/upload/" . $filename;
    }
}