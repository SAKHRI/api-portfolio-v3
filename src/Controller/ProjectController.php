<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use App\Service\FileService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * @Route("/project", name="project")
 */

class ProjectController extends Controller
{
    private $serializer;
    public function __construct(SerializerInterface $serializer)

    {
        $this->serializer = $serializer;
    }


    /**
     * @Route("/", methods={"GET"})
     */


    public function all()
    {
        // //On crée une réponse http
        // $response = new Response();
        // //On indique que son contenu sera du json
        // $response->headers->set("Content-Type", "application/json");
        // //On indique que le code de retour est 200 ok
        // $response->setStatusCode(200);
        // //On met comme contenu un petit tableau encodé en json
        // $response->setContent(json_encode(["ga" => "bu"]));
        
        $repo = $this->getDoctrine()->getRepository(Project::class);
        $projects = $repo->findAll();
        //On utilise le serializer de symfony pour transfomer une
        //entité (ou array d'entités) php au format json
        $json = $this->serializer->serialize($projects, "json");
        //On utilise la méthode static fromJsonString de la classe
        //JsonResponse pour créer une réponse HTTP en json à partir
        //de données déjà au format JSON
        return JsonResponse::fromJsonString($json);
    }
    
    /**
     * @Route("/", methods={"POST"})
     */
    public function add(Request $req, FileService $fileService) {
        
        $image = $req->files->get("url");
        $absoluteUrl = $req->getScheme() . '://' . $req->getHttpHost() . $req->getBasePath();
        $imageUrl = $fileService->upload($image, $absoluteUrl);

        $project = new Project;
        $project->setTitre($req->get("titre"));
        $project->setDescription($req->get("description"));
        $project->setUrl($imageUrl);
        $project->setUrlSimplon($req->get("urlSimplon"));
        $project->setUrlGitlab($req->get("urlGitlab"));

    
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($project);
        $manager->flush();

        
        return new JsonResponse(["id" => $project->getId()]);
    }



    /**
     * @Route("/{project}", methods={"GET"})
     */
    public function single(Project $project) {
        $json = $this->serializer->serialize($project,"json");
        return JsonResponse::fromJsonString($json);
    }



    /**
     * @Route("/update/{project}", methods={"PUT"})
     */
    public function update(Project $project, Request $request) {
        $body = $request->getContent();
        $updated = $this->serializer->deserialize($body, Project::class, "json");

        $manager = $this->getDoctrine()->getManager();


        
        $project->setTitre($updated->getTitre());
        $project->setDescription($updated->getDescription());
        $project->setUrl($imageUrl);
        $project->setUrlSimplon($updated->getUrlSimplon());
        $project->setUrlGitlab($updated->getUrlGitlab());


        
        $manager->flush();
        return new Response("", 204);
    }




    /**
     * @Route("/delete/{project}", methods={"DELETE"})
     */

    public function remove(Project $project) {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($project);
        $manager->flush();
        return new Response("", 204);
    }


}

